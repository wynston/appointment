import express from 'express';
const router = express.Router();


var webLogin = require('web-wechat-login');

var wechat = new webLogin({
    appId : 'wxe257ea80b424f6f7',
    appSecret:'5521ab730200b27a2494e02ca9a39059',
    callbackUrl: 'https://wechat.izanmi.com/wechat/oauth/wechat/callback'
});


/* GET wechat login page. */
router.get('/login', function (req, res) {
  console.log('login');
    wechat.login(req, res);
});

/* GET wechat authorize page. */
router.get('/authorize', function (req, res) {
    wechat.authorize(req, res);
});


/* listen wechat callback. */
router.all('/callback', function (req, res) {
    wechat.callback(req, res).then(function (result) {
            var user = {
                openid    : result.openid,
                unionid   : result.unionid,
                nickname  : result.nickname,
                headimgurl: result.headimgurl,
                sex       : result.sex
            };
            req.session.user = user;
            res.redirect('/')
        }).catch(function (err) {
            res.send('err' + err);
        });
});

export default router;
