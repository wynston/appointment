import express from 'express';
import ssr from './ssr';
import wechat from './wechat';

const app = express();

app.set('view engine', 'ejs');
app.use('/wechat', wechat);
app.use(express.static('public'));
app.use('/*', ssr);

app.listen(3007, () => {
  console.log('Hello World listening on port 3007!');
});
